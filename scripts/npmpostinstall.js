console.log('\n----- npm postinstall script start -----\n');

var fs = require('fs-extra');
var child_process = require('child_process');

function dirExists(dir) {
    try {
        fs.accessSync(dir, fs.F_OK);
        return true;
    } catch (e) {
        return false;
    }
}

if (dirExists('episode_generator')) {
	console.log('Running "npm install" for episode_generator folder...');
	child_process.execSync('npm install', { cwd: './episode_generator', stdio: 'inherit' });
}

console.log('\n----- npm postinstall script end -----\n');