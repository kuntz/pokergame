'use strict';

define([
    'resources/ivds'
    ], function(ivds) {
        var nodes = [
            {   
                id: '00A_Spark',
                source: ivds.vid_00A_Spark,
                data: {
                    decision: {
                        children: ['01A_Intro'],
                        defaults: ['01A_Intro']
                    }
                }
            },
            {
                id:'01A_Intro',
                source:ivds.vid_01A_Intro,
                data: {
                    decision: {
                        children: ['02A_Loop_of_whole_table'],
                        defaults: ['02A_Loop_of_whole_table']
                    }
                }
            },
            {
                id:'02A_Loop_of_whole_table',
                source:ivds.vid_02A_Loop_of_whole_table,
                data: {
                    decision: {
                        children: ['03A_Deals_to_Illana'],
                        defaults: ['03A_Deals_to_Illana']
                    }
                }
            },
            {
                id:'03A_Deals_to_Illana',
                dealToPlayer: true,
                player: 'ilana',
                type: 'dealToPlayer',
                source:ivds.vid_03A_Deals_to_Illana,
                data: {
                    decision: {
                        children: ['04A_ILLANA_looks_at_her_cards_happy', '04B_ILLANA_looks_at_her_cards_sad'],
                        defaults: ['04A_ILLANA_looks_at_her_cards_happy']
                    }
                }
            },
            {
                id:'04A_ILLANA_looks_at_her_cards_happy',
                source:ivds.vid_04A_ILLANA_looks_at_her_cards_happy,
                data: {
                    decision: {
                        children: ['05A_DEALS_TO_BOB'],
                        defaults: ['05A_DEALS_TO_BOB']
                    }
                }
            },
            {
                id:'04B_ILLANA_looks_at_her_cards_sad',
                source:ivds.vid_04B_ILLANA_looks_at_her_cards_sad,
                data: {
                    decision: {
                        children: ['05A_DEALS_TO_BOB'],
                        defaults: ['05A_DEALS_TO_BOB']
                    }
                }
            },
            {
                id:'05A_DEALS_TO_BOB',
                dealToPlayer: true,
                type: 'dealToPlayer',
                player: 'bob',
                source:ivds.vid_05A_DEALS_TO_BOB,
                data: {
                    decision: {
                        children: ['06A_BOB_looks_at_cards_happy', '06B_BOB_looks_at_cards_sad'],
                        defaults: ['06A_BOB_looks_at_cards_happy']
                    }
                }
            },
            {
                id:'06A_BOB_looks_at_cards_happy',
                source:ivds.vid_06A_BOB_looks_at_cards_happy,
                data: {
                    decision: {
                        children: ['07A_DEALS_TO_CAROL'],
                        defaults: ['07A_DEALS_TO_CAROL']
                    }
                }
            },
            {
                id:'06B_BOB_looks_at_cards_sad',
                source:ivds.vid_06B_BOB_looks_at_cards_sad,
                data: {
                    decision: {
                        children: ['07A_DEALS_TO_CAROL'],
                        defaults: ['07A_DEALS_TO_CAROL']
                    }
                }
            },
            {
                id:'07A_DEALS_TO_CAROL',
                type: 'dealToPlayer',
                dealToPlayer: true,
                player: 'carol',
                source:ivds.vid_07A_DEALS_TO_CAROL,
                data: {
                    decision: {
                        children: ['08A_CAROL_looks_at_cards_happy', '08B_CAROL_looks_at_cards_sad'],
                        defaults: ['08A_CAROL_looks_at_cards_happy']
                    }
                }
            },
            {
                id:'08A_CAROL_looks_at_cards_happy',
                source:ivds.vid_08A_CAROL_looks_at_cards_happy,
                type: 'opponentsFeelings',
                data: {
                    decision: {
                        children: ['08ZA_THE_THREE_OPPONENTS_FEEL_GOOD_ABOUT_THIS', '08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS'],
                        defaults: ['08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS']
                    }
                }
            },
            {
                id:'08B_CAROL_looks_at_cards_sad',
                type: 'opponentsFeelings',
                source:ivds.vid_08B_CAROL_looks_at_cards_sad,
                data: {
                    decision: {
                        children: ['08ZA_THE_THREE_OPPONENTS_FEEL_GOOD_ABOUT_THIS', '08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS'],
                        defaults: ['08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS']
                    }
                }
            },
            {
                id:'08ZA_THE_THREE_OPPONENTS_FEEL_GOOD_ABOUT_THIS',
                source:ivds.vid_08ZA_THE_THREE_OPPONENTS_FEEL_GOOD_ABOUT_THIS,
                data: {
                    decision: {
                        children: ['09A_DEAL_TO_ALEX'],
                        defaults: ['09A_DEAL_TO_ALEX']
                    }
                }
            },
            {
                id:'08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS',
                source:ivds.vid_08ZB_THE_THREE_OPPONENTS_FEEL_BAD_ABOUT_THIS,
                data: {
                    decision: {
                        children: ['09A_DEAL_TO_ALEX'],
                        defaults: ['09A_DEAL_TO_ALEX']
                    }
                }
            },
            {
                id:'09A_DEAL_TO_ALEX',
                type: 'dealToPlayer',
                dealToPlayer: true,
                player: 'alex',
                source:ivds.vid_09A_DEAL_TO_ALEX,
                data: {
                    decision: {
                        children: ['10A_ALEX_looks_at_cards_happy', '10B_ALEX_looks_at_cards_sad']
                    }
                }
            },
            {
                id:'10A_ALEX_looks_at_cards_happy',
                source:ivds.vid_10A_ALEX_looks_at_cards_happy,
                data: {
                    decision: {
                        children: ['11A_Loop_of_whole_table_before_flop'],
                        defaults: ['11A_Loop_of_whole_table_before_flop']
                    }
                }
            },
            {
                id:'10B_ALEX_looks_at_cards_sad',
                source:ivds.vid_10B_ALEX_looks_at_cards_sad,
                data: {
                    decision: {
                        children: ['11A_Loop_of_whole_table_before_flop'],
                        defaults: ['11A_Loop_of_whole_table_before_flop']
                    }
                }
            },
            {
                id:'11A_Loop_of_whole_table_before_flop',
                type: 'newRound',
                source:ivds.vid_11A_Loop_of_whole_table_before_flop,
                data: {
                    decision: {
                        children: ['12A_DEALS_FLOP'],
                        defaults: ['12A_DEALS_FLOP']
                    }
                }
            },
            {
                id:'12A_DEALS_FLOP',
                type: 'playAfterFlop',
                dealFlop: true,
                nextPlayer: 'ilana',
                source:ivds.vid_12A_DEALS_FLOP,
                data: {
                    decision: {
                        children: ['13A_ILLANA_bets', '13B_ILLANA_raises', '13C_ILLANA_folds'],
                        defaults: ['13A_ILLANA_bets']
                    }
                }
            },
            {
                id:'13A_ILLANA_bets',
                type: 'playAfterFlop',
                player: 'ilana',
                nextPlayer: 'bob',
                source:ivds.vid_13A_ILLANA_bets,
                data: {
                    decision: {
                        children: ['14A_BOB_bets', '14B_BOB_raises', '14C_BOB_folds'],
                        defaults: ['14A_BOB_bets']
                    }
                }
            },
            {
                id:'13B_ILLANA_raises',
                type: 'playAfterFlop',
                player: 'ilana',
                nextPlayer: 'bob',
                source:ivds.vid_13B_ILLANA_raises,
                data: {
                    decision: {
                        children: ['14A_BOB_bets', '14B_BOB_raises', '14C_BOB_folds'],
                        defaults: ['14A_BOB_bets']
                    }
                }
            },
            {
                id:'13C_ILLANA_folds',
                type: 'playAfterFlop',
                player: 'ilana',
                nextPlayer: 'bob',
                source:ivds.vid_13C_ILLANA_folds,
                data: {
                    decision: {
                        children: ['14A_BOB_bets', '14B_BOB_raises', '14C_BOB_folds'],
                        defaults: ['14A_BOB_bets']
                    }
                }
            },
            {
                id:'14A_BOB_bets',
                type: 'playAfterFlop',
                player: 'bob',
                nextPlayer: 'carol',
                source:ivds.vid_14A_BOB_bets,
                data: {
                    decision: {
                        children: ['15A_CAROL_bets', '15B_CAROL_raises', '15C_CAROL_folds'],
                        defaults: ['15A_CAROL_bets']
                    }
                }
            },
            {
                id:'14B_BOB_raises',
                type: 'playAfterFlop',
                player: 'bob',
                nextPlayer: 'carol',
                source:ivds.vid_14B_BOB_raises,
                data: {
                    decision: {
                        children: ['15A_CAROL_bets', '15B_CAROL_raises', '15C_CAROL_folds'],
                        defaults: ['15A_CAROL_bets']
                    }
                }
            },
            {
                id:'14C_BOB_folds',
                type: 'playAfterFlop',
                player: 'bob',
                nextPlayer: 'carol',
                source:ivds.vid_14C_BOB_folds,
                data: {
                    decision: {
                        children: ['15A_CAROL_bets', '15B_CAROL_raises', '15C_CAROL_folds'],
                        defaults: ['15A_CAROL_bets']
                    }
                }
            },
            {
                id:'15A_CAROL_bets',
                player: 'carol',
                nextPlayer: 'alex',
                type: 'playAfterFlop',
                source:ivds.vid_15A_CAROL_bets,
                data: {
                    decision: {
                        children: ['16A_ALEX_bets', '16B_ALEX_folds'],
                        defaults: ['16A_ALEX_bets']
                    }
                }
            },
            {
                id:'15B_CAROL_raises',
                player: 'carol',
                nextPlayer: 'alex',
                type: 'playAfterFlop',
                source:ivds.vid_15B_CAROL_raises,
                data: {
                    decision: {
                        children: ['16A_ALEX_bets', '16B_ALEX_folds'],
                        defaults: ['16A_ALEX_bets']
                    }
                }
            },
            {
                id:'15C_CAROL_folds',
                player: 'carol',
                nextPlayer: 'alex',
                type: 'playAfterFlop',
                source:ivds.vid_15C_CAROL_folds,
                data: {
                    decision: {
                        children: ['16A_ALEX_bets', '16B_ALEX_folds', '17A_GAME_IS_OVER'],
                        defaults: ['16A_ALEX_bets']
                    }
                }
            },
            {
                id:'16A_ALEX_bets',
                player: 'alex',
                source:ivds.vid_16A_ALEX_bets,
                data: {
                    decision: {
                        children: ['18A_Devil_Comment1', '18B_Devil_Comment2', '18C_Devil_Comment3'],
                        defaults: ['18A_Devil_Comment1', '18B_Devil_Comment2', '18C_Devil_Comment3']
                    }
                }
            },
            {
                id:'16B_ALEX_folds',
                player: 'alex',
                source:ivds.vid_16B_ALEX_folds,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'17A_GAME_IS_OVER',
                source:ivds.vid_17A_GAME_IS_OVER,
                data: {
                    decision: {
                        children :['34A_CODA']
                    }
                }
            },
            {
                id:'18A_Devil_Comment1',
                type: 'beforeLoopOfTable',
                source:ivds.vid_18A_Devil_Comment1,
                data: {
                    decision: {
                        children: ['19A_Loop_of_whole_table_before_4th_St', '19B_Loop_of_whole_table_before_4th_St_MINUS_ILLANA', '19C_Loop_of_whole_table_before_4th_St_MINUS_BOB', '19D_Loop_of_whole_table_before_4th_St_MINUS_CAROL']
                    }
                }
            },
            {
                id:'18B_Devil_Comment2',
                type: 'beforeLoopOfTable',
                source:ivds.vid_18B_Devil_Comment2,
                data: {
                    decision: {
                        children: ['19A_Loop_of_whole_table_before_4th_St', '19B_Loop_of_whole_table_before_4th_St_MINUS_ILLANA', '19C_Loop_of_whole_table_before_4th_St_MINUS_BOB', '19D_Loop_of_whole_table_before_4th_St_MINUS_CAROL']
                    }
                }
            },
            {
                id:'18C_Devil_Comment3',
                type: 'beforeLoopOfTable',
                source:ivds.vid_18C_Devil_Comment3,
                data: {
                    decision: {
                        children: ['19A_Loop_of_whole_table_before_4th_St', '19B_Loop_of_whole_table_before_4th_St_MINUS_ILLANA', '19C_Loop_of_whole_table_before_4th_St_MINUS_BOB', '19D_Loop_of_whole_table_before_4th_St_MINUS_CAROL']
                    }
                }
            },
            {
                id:'19A_Loop_of_whole_table_before_4th_St',
                type: 'newRound',
                source:ivds.vid_19A_Loop_of_whole_table_before_4th_St,
                data: {
                    decision: {
                        children: ['20A_DEALS_STREET'],
                        defaults: ['20A_DEALS_STREET']
                    }
                }
            },
            {
                id:'19B_Loop_of_whole_table_before_4th_St_MINUS_ILLANA',
                type: 'newRound',
                source:ivds.vid_19B_Loop_of_whole_table_before_4th_St_MINUS_ILLANA,
                data: {
                    decision: {
                        children: ['20A_DEALS_STREET'],
                        defaults: ['20A_DEALS_STREET']
                    }
                }
            },
            {
                id:'19C_Loop_of_whole_table_before_4th_St_MINUS_BOB',
                type: 'newRound',
                source:ivds.vid_19C_Loop_of_whole_table_before_4th_St_MINUS_BOB,
                data: {
                    decision: {
                        children: ['20A_DEALS_STREET'],
                        defaults: ['20A_DEALS_STREET']
                    }
                }
            },
            {
                id:'19D_Loop_of_whole_table_before_4th_St_MINUS_CAROL',
                type: 'newRound',
                source:ivds.vid_19D_Loop_of_whole_table_before_4th_St_MINUS_CAROL,
                data: {
                    decision: {
                        children: ['20A_DEALS_STREET'],
                        defaults: ['20A_DEALS_STREET']
                    }
                }
            },
            {
                id:'20A_DEALS_STREET',
                dealStreet: true,
                type: "dealStreet",
                nextPlayers: ['ilana', 'bob', 'carol', 'alex'],
                playerNodesMap: {
                    ilana: [0,1,2],
                    bob: [3,4,5],
                    carol: [6,7,8],
                    alex: [9,10]
                },
                source:ivds.vid_20A_DEALS_STREET,
                data: {
                    decision: {
                        children: ['21A_ILLANA_bets', '21B_ILLANA_raises', '21C_ILLANA_folds', '22A_BOB_bets', '22B_BOB_raises', '22C_BOB_folds', '23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'21A_ILLANA_bets',
                player: 'ilana',
                type: "dealStreet",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7]
                },
                source:ivds.vid_21A_ILLANA_bets,
                data: {
                    decision: {
                        children: ['22A_BOB_bets', '22B_BOB_raises', '22C_BOB_folds', '23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'21B_ILLANA_raises',
                player: 'ilana',
                type: "dealStreet",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7]
                },
                source:ivds.vid_21B_ILLANA_raises,
                data: {
                    decision: {
                        children: ['22A_BOB_bets', '22B_BOB_raises', '22C_BOB_folds', '23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'21C_ILLANA_folds',
                player: 'ilana',
                type: "dealStreet",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7]
                },
                source:ivds.vid_21C_ILLANA_folds,
                data: {
                    decision: {
                        children: ['22A_BOB_bets', '22B_BOB_raises', '22C_BOB_folds', '23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds', '25A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'22A_BOB_bets',
                player: 'bob',
                type: "dealStreet",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4]
                },
                source:ivds.vid_22A_BOB_bets,
                data: {
                    decision: {
                        children: ['23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'22B_BOB_raises',
                player: 'bob',
                type: "dealStreet",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4]
                },
                source:ivds.vid_22B_BOB_raises,
                data: {
                    decision: {
                        children: ['23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'22C_BOB_folds',
                player: 'bob',
                type: "dealStreet",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4]
                },
                source:ivds.vid_22C_BOB_folds,
                data: {
                    decision: {
                        children: ['23A_CAROL_bets', '23B_CAROL_raises', '23C_CAROL_folds', '24A_ALEX_bets', '24B_ALEX_folds', '25A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'23A_CAROL_bets',
                player: 'carol',
                type: "dealStreet",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1]
                },
                source:ivds.vid_23A_CAROL_bets,
                data: {
                    decision: {
                        children: ['24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'23B_CAROL_raises',
                player: 'carol',
                type: "dealStreet",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1]
                },
                source:ivds.vid_23B_CAROL_raises,
                data: {
                    decision: {
                        children: ['24A_ALEX_bets', '24B_ALEX_folds']
                    }
                }
            },
            {
                id:'23C_CAROL_folds',
                player: 'carol',
                type: "dealStreet",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1]
                },
                source:ivds.vid_23C_CAROL_folds,
                data: {
                    decision: {
                        children: ['24A_ALEX_bets', '24B_ALEX_folds', '25A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'24A_ALEX_bets',
                player: 'alex',
                source:ivds.vid_24A_ALEX_bets,
                data: {
                    decision: {
                        children: ['26A_Devil_Comment1', '26B_Devil_Comment2', '26C_Devil_Comment3'],
                        defaults: ['26A_Devil_Comment1', '26B_Devil_Comment2', '26C_Devil_Comment3']
                    }
                }
            },
            {
                id:'24B_ALEX_folds',
                player: 'alex',
                source:ivds.vid_24B_ALEX_folds,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'25A_GAME_IS_OVER',
                source:ivds.vid_25A_GAME_IS_OVER,
                data: {
                    decision: {
                        children :['34A_CODA']
                    }
                }
            },
            {
                id:'26A_Devil_Comment1',
                type: 'beforeLoopOfTable',
                source:ivds.vid_26A_Devil_Comment1,
                data: {
                    decision: {
                        children: ['27A_Loop_of_whole_table_before_river', '27B_Loop_of_whole_table_before_river_MINUS_ILLANA', '27C_Loop_of_whole_table_before_river_MINUS_BOB', '27D_Loop_of_whole_table_before_river_MINUS_CAROL', '27E_Loop_of_whole_table_before_river_MINUS_CAROL_and_BOB', '27F_Loop_of_whole_table_before_river_MINUS_CAROL_and_ILLANA', '27G_Loop_of_whole_table_before_river_MINUS_BOB_and_ILLANA']
                    }
                }
            },
            {
                id:'26B_Devil_Comment2',
                type: 'beforeLoopOfTable',
                source:ivds.vid_26B_Devil_Comment2,
                data: {
                    decision: {
                        children: ['27A_Loop_of_whole_table_before_river', '27B_Loop_of_whole_table_before_river_MINUS_ILLANA', '27C_Loop_of_whole_table_before_river_MINUS_BOB', '27D_Loop_of_whole_table_before_river_MINUS_CAROL', '27E_Loop_of_whole_table_before_river_MINUS_CAROL_and_BOB', '27F_Loop_of_whole_table_before_river_MINUS_CAROL_and_ILLANA', '27G_Loop_of_whole_table_before_river_MINUS_BOB_and_ILLANA']
                    }
                }
            },
            {
                id:'26C_Devil_Comment3',
                type: 'beforeLoopOfTable',
                source:ivds.vid_26C_Devil_Comment3,
                data: {
                    decision: {
                        children: ['27A_Loop_of_whole_table_before_river', '27B_Loop_of_whole_table_before_river_MINUS_ILLANA', '27C_Loop_of_whole_table_before_river_MINUS_BOB', '27D_Loop_of_whole_table_before_river_MINUS_CAROL', '27E_Loop_of_whole_table_before_river_MINUS_CAROL_and_BOB', '27F_Loop_of_whole_table_before_river_MINUS_CAROL_and_ILLANA', '27G_Loop_of_whole_table_before_river_MINUS_BOB_and_ILLANA']
                    }
                }
            },
            {
                id:'27A_Loop_of_whole_table_before_river',
                type: 'newRound',
                source:ivds.vid_27A_Loop_of_whole_table_before_river,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27B_Loop_of_whole_table_before_river_MINUS_ILLANA',
                type: 'newRound',
                source:ivds.vid_27B_Loop_of_whole_table_before_river_MINUS_ILLANA,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27C_Loop_of_whole_table_before_river_MINUS_BOB',
                type: 'newRound',
                source:ivds.vid_27C_Loop_of_whole_table_before_river_MINUS_BOB,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27D_Loop_of_whole_table_before_river_MINUS_CAROL',
                type: 'newRound',
                source:ivds.vid_27D_Loop_of_whole_table_before_river_MINUS_CAROL,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27E_Loop_of_whole_table_before_river_MINUS_CAROL_and_BOB',
                type: 'newRound',
                source:ivds.vid_27E_Loop_of_whole_table_before_river_MINUS_CAROL_and_BOB,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27F_Loop_of_whole_table_before_river_MINUS_CAROL_and_ILLANA',
                type: 'newRound',
                source:ivds.vid_27F_Loop_of_whole_table_before_river_MINUS_CAROL_and_ILLANA,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'27G_Loop_of_whole_table_before_river_MINUS_BOB_and_ILLANA',
                type: 'newRound',
                source:ivds.vid_27G_Loop_of_whole_table_before_river_MINUS_BOB_and_ILLANA,
                data: {
                    decision: {
                        children: ['28A_DEALS_RIVER'],
                        defaults: ['28A_DEALS_RIVER']
                    }
                }
            },
            {
                id:'28A_DEALS_RIVER',
                dealRiver: true,
                type: 'dealRiver',
                nextPlayers: ['ilana', 'bob', 'carol', 'alex'],
                playerNodesMap: {
                    ilana: [0,1,2],
                    bob: [3,4,5],
                    carol: [6,7,8],
                    alex: [9, 10]
                },
                source:ivds.vid_28A_DEALS_RIVER,
                data: {
                    decision: {
                        children: ['29A_ILLANA_bets', '29B_ILLANA_raises', '29C_ILLANA_folds', '30A_BOB_bets', '30B_BOB_raises', '30C_BOB_folds', '31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'29A_ILLANA_bets',
                player: 'ilana',
                type: "dealRiver",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7]
                },
                source:ivds.vid_29A_ILLANA_bets,
                data: {
                    decision: {
                        children: ['30A_BOB_bets', '30B_BOB_raises', '30C_BOB_folds', '31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'29B_ILLANA_raises',
                player: 'ilana',
                type: "dealRiver",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7]
                },
                source:ivds.vid_29B_ILLANA_raises,
                data: {
                    decision: {
                        children: ['30A_BOB_bets', '30B_BOB_raises', '30C_BOB_folds', '31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'29C_ILLANA_folds',
                player: 'ilana',
                type: "dealRiver",
                nextPlayers: ['bob', 'carol', 'alex'],
                playerNodesMap: {
                    bob: [0,1,2],
                    carol: [3,4,5],
                    alex: [6,7],
                    endOfGame: 8
                },
                source:ivds.vid_29C_ILLANA_folds,
                data: {
                    decision: {
                        children: ['30A_BOB_bets', '30B_BOB_raises', '30C_BOB_folds', '31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds', '315A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'30A_BOB_bets',
                player: 'bob',
                type: "dealRiver",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4]
                },
                source:ivds.vid_30A_BOB_bets,
                data: {
                    decision: {
                        children: ['31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'30B_BOB_raises',
                player: 'bob',
                type: "dealRiver",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4]
                },
                source:ivds.vid_30B_BOB_raises,
                data: {
                    decision: {
                        children: ['31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'30C_BOB_folds',
                player: 'bob',
                type: "dealRiver",
                nextPlayers: ['carol', 'alex'],
                playerNodesMap: {
                    carol: [0,1,2],
                    alex: [3,4],
                    endOfGame: 5  
                },
                source:ivds.vid_30C_BOB_folds,
                data: {
                    decision: {
                        children: ['31A_CAROL_bets', '31B_CAROL_raises', '31C_CAROL_folds', '32A_ALEX_bets', '32B_ALEX_folds', '315A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'31A_CAROL_bets',
                player: 'carol',
                type: "dealRiver",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1],
                },
                source:ivds.vid_31A_CAROL_bets,
                data: {
                    decision: {
                        children: ['32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'31B_CAROL_raises',
                player: 'carol',
                type: "dealRiver",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1],
                },
                source:ivds.vid_31B_CAROL_raises,
                data: {
                    decision: {
                        children: ['32A_ALEX_bets', '32B_ALEX_folds']
                    }
                }
            },
            {
                id:'31C_CAROL_folds',
                player: 'carol',
                type: "dealRiver",
                nextPlayers: ['alex'],
                playerNodesMap: {
                    alex: [0,1],
                    endOfGame: 2
                },
                source:ivds.vid_31C_CAROL_folds,
                data: {
                    decision: {
                        children: ['32A_ALEX_bets', '32B_ALEX_folds', '315A_GAME_IS_OVER']
                    }
                }
            },
            {
                id:'315A_GAME_IS_OVER',
                source:ivds.vid_25A_GAME_IS_OVER,
                data: {
                    decision: {
                        children :['34A_CODA']
                    }
                }
            },
            {
                id:'32A_ALEX_bets',
                player: 'alex',
                type: 'matchPoint',
                alexFolded: false,
                source:ivds.vid_32A_ALEX_bets,
                data: {
                    decision: {
                        children: ['33A_ALEX_wins', '33B_ALEX_loses_ILLANA_wins', '33C_ALEX_loses_CAROL_wins', '33D_ALEX_loses_BOB_wins']
                    }
                }
            },
            {
                id:'32B_ALEX_folds',
                player: 'alex',
                type: 'matchPoint',
                alexFolded: true,
                source:ivds.vid_32B_ALEX_folds,
                data: {
                    decision: {
                        children: ['33B_ALEX_loses_ILLANA_wins', '33C_ALEX_loses_CAROL_wins', '33D_ALEX_loses_BOB_wins']
                    }
                }
            },
            {
                id:'33A_ALEX_wins',
                source:ivds.vid_33A_ALEX_wins,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'33B_ALEX_loses_ILLANA_wins',
                source:ivds.vid_33B_ALEX_loses_ILLANA_wins,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'33C_ALEX_loses_CAROL_wins',
                source:ivds.vid_33C_ALEX_loses_CAROL_wins,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'33D_ALEX_loses_BOB_wins',
                source:ivds.vid_33D_ALEX_loses_BOB_wins,
                data: {
                    decision: {
                        children: ['34A_CODA']
                    }
                }
            },
            {
                id:'34A_CODA',
                source:ivds.vid_34A_CODA,
                data: {}
            }];
    return nodes;
    }
);