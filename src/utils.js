define([
	'underscore',
	'text!config/settings.json'
	],
	function(_, settings) {

		settings = JSON.parse(settings);
		var ga = window.ga;

        // uses the tracker that was defined in the Google Analytics script in embed.html to send events to GA
        var sendGaEvent = function (category, action) {
            if (typeof ga === 'function')
                ga('dashboardTracker.send', 'event', category, action, settings.id);
        };

		var fixedEncodeURIComponent = function(str) {
	        return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
	            return '%' + c.charCodeAt(0).toString(16);
	        });
	    };

	    var getSearch = function(object) {
	        // object = _.mapObject(object, fixedEncodeURIComponent);
	        var search = '?';
	        for (var key in object) {
	            if (object.hasOwnProperty(key) && object[key]!=='') {
	                if (search.length > 1) {
	                    search += '&';
	                }
	                search += key + '=' + object[key];
	            }
	        }
	        return search;

	    };

	    var checkEnvironment = function(InterludePlayer) {
	        var env = InterludePlayer.environment;
	        var guiSupport = !(env.os.name === 'Windows' && env.browser.name === 'IE' && parseInt(env.browser.major) < 11);

	        // If player is supported on current device/environment, start player
	        if (InterludePlayer.isSupported() && guiSupport) {
	            return true;
	        }

	        // Otherwise, if player is not supported in current environment --> redirect to fallback page
	        else {
	            var iosParams =  {
	                autoplay: true
	            };

	            var urlParams = {
	                html_url: settings.embedUrl,
	                bg: settings.backgroundImage,
	                thumb: settings.thumbnailUrl,
	                title: fixedEncodeURIComponent(settings.title),
	                projectId: fixedEncodeURIComponent(settings.id)
	            };
	            iosParams = _.extend(iosParams, env.queryParams);
	            urlParams.html_url += fixedEncodeURIComponent(getSearch(iosParams));
	            console.log(urlParams.html_url);
	            var qs = getSearch(urlParams);
	            // window.location = 'http://stage.interlude.fm/fallback/' + qs;
	            window.location = 'http://stage.interlude.fm/fallback/' + qs;
	        }
	    };

	    var getHiddenProp = function() {
            var prefixes = ['webkit', 'moz', 'ms', 'o'];

            // if 'hidden' is natively supported just return it
            if ('hidden' in document) {
                return 'hidden';
            }

            // otherwise loop over all the known prefixes until we find one
            for (var i = 0; i < prefixes.length; i++) {
                if ((prefixes[i] + 'Hidden') in document) {
                    return prefixes[i] + 'Hidden';
                }
            }

            // otherwise it's not supported
            return null;
        };

	    var isHidden = function() {
            var prop = getHiddenProp();
            if (!prop) {
                return false;
            }
            return document[prop];
        };

        var visChange = function(interlude) {
            if (!isHidden()) {
                interlude.player.seek.stop();
            }
        };

	    var refreshHLS = function(interlude) {
	    	/*** refresh page if tab out of focus on hls engine ***/
		    var visProp = getHiddenProp();
	        if (visProp) {
	            var evt = visProp.replace(/[H|h]idden/, '') + 'visibilitychange';
	            document.addEventListener(evt, visChange.bind(this, interlude));
	        }
	    };

	    var handleFallbackMessage = function(e) {
	    	var url = (e.origin.substring(0,5) === 'https') ? 'http' + e.origin.substring(5) : e.origin;
            if(url === 'http://stage.interlude.fm') {
                switch (e.data.action) {
                    case 'ga':
                        var eventName = e.data.eventName;
                        var label = settings.id;
                        sendGaEvent('dashboardTracker.send', 'event', 'player', eventName, label);
                        break;
                }
            }
	    };

		return {
			fixedEncodeURIComponent: fixedEncodeURIComponent,
    		getSearch: getSearch,
    		checkEnvironment: checkEnvironment,
    		refreshHLS: refreshHLS,
    		sendGaEvent: sendGaEvent,
    		handleFallbackMessage: handleFallbackMessage
    	};
	}
);
