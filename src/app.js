define([
    'resources/ivds',
    'resources/nodes',
    'text!config/settings.json',
    'text!resources/skins.json',
    'resources/skins',
    'js/overlays',
    'js/NoLuck',
    'text!stylesheets/cardDeck.css',
    'text!stylesheets/baraja.css',
    'text!stylesheets/demo.css',
    'text!stylesheets/custom.css'
    ],
function(ivds, nodes, settings, skinsJSON, skinsJS, overlays, NoLuck, cardDeckCss, barajaCss, demoCss, customCss) {

    'use strict';

    // app.js variables
    // hold a reference to the Interlude Player
    var player;
    var interlude;
    var head = '01A_Intro';
    settings = JSON.parse(settings);

    var noLuckStory;

    function onNodeStart() {
        console.log('node has started');
    }

    // project essentials
    return {
        // the following are expected to be provided for Helix
        settings: settings,

        // Will be called by Helix and is responsible to preare the player
        // for playback of the app (e.g. adding nodes, GUI, etc')
        onInit: function(interludeRef) {
            player = interludeRef.player;
            interlude = interludeRef;

            noLuckStory = new NoLuck({
                player: player
            });
            window.noLuck = noLuckStory;

            // nodes
            player.repository.add(nodes);

            // decisions
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].data && nodes[i].data.decision && nodes[i].data.decision.children && nodes[i].data.decision.children.length > 0) {
                    player.decision.add(nodes[i].id, {decider: noLuckStory.getDecisionFunctions(nodes[i])});
                }
            }

            // Graph
            player.graph.setHead(head);
            noLuckStory.addNodeListeners(nodes);
            

            //var jQuery = document.createElement('script');
            //jQuery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js';
            //document.head.appendChild(jQuery);
            //
            //// Adding jQuery plugin
            //var jQueryPlugin = document.createElement('script');
            //jQueryPlugin.src = 'resources/modernizr.custom.79639.js';
            //jQueryPlugin.type = 'text/javascript';
            //document.head.appendChild(jQueryPlugin);

            // Add css files
            var cssFiles = [cardDeckCss, barajaCss, demoCss, customCss];
            for (var i = 0; i < cssFiles.length; i++) {
                var style = document.createElement('style');
                style.type = 'text/css';
                style.innerHTML = cssFiles[i];
                document.head.appendChild(style);
            }

            // GUI
            player.gui.addSkin({
                id: 'defaultIndication',
                url: 'https://d1w2zhnqcy4l8f.cloudfront.net/treehouseResources/parallel_indications/defaultParallelIndication.iskn'
            });
            player.gui.add(
                player.uitools.React.DOM.div({
                        id: 'parallelIndication',
                        zIndex: 3,
                        scaling: false,
                        boxing: false,
                        style: {
                            width: '100%',
                            height: '100%',
                            visibility: 'visible'
                        }
                    },
                    player.gui.InterludeParallelIndication({
                        id: 'parallelIndication',
                        skinSource: 'defaultIndication'
                    })
                )
            );
            if (JSON.parse(skinsJSON).length > 0) {
                player.gui.addSkin(JSON.parse(skinsJSON));
            }
            if (Object.keys(skinsJS).length > 0) {
                // Map the skinJS object into an array of skin objects (where "id"= key)
                var skinsArray = Object.keys(skinsJS).map(function(key) {
                    skinsJS[key].id = key;
                    return skinsJS[key];
                });
                player.gui.addSkin(skinsArray);
            }
            player.gui.add(overlays.overlays);

        },

        // the ID for the head node to be loaded by Helix. This can also be
        // a function.
        head: head,

        // Will be called automatically at the start of the headnode.
        onStarted: function() {
            // Audio
            player.audio.add({
                id: 'partial_soundtrack', src: ['https://www.freesound.org/data/previews/198/198922_2440401-lq.mp3', 'https://s3.amazonaws.com/storage.interlude.fm/lucknomore/audio/198922_2440401_lq.ogg'], bindVolumeToPlayer: false, loop: true, volume: 0.2
            });
            noLuckStory.setStartScreen();
            player.on('nodestart', onNodeStart, 'demo');
        },

        // Will be called by Helix once the project is about to reach its end.
        // Responsible for cleanup.
        onEnd: function() {
            player.audio.stop('partial_soundtrack');
            noLuckStory.resetLocals();
            // do something...
            player.off(null, null, 'demo');
        }
    };
});
