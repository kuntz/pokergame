'use strict';
define([
        'text!js/views/cardDeck.html',
        'text!js/views/selectCardsButton.html',
        'text!js/views/cardDeck.html',
        'text!js/views/singleCardInput.html',
        'js/eventEmitter',
        'js/heir'
    ],

    function (deckTemplate, showCardsButtonTemplate, selectCardsSimpleTemplate, singleCardInput, EventEmitter, Heir) {

        var SELECT_CARDS_OVERLAY = "deck_overlay";
        var SHOW_CARDS_BUTTON_OVERLAY = "show_cards_button_overlay";

        var CardDeckGui = function (player, cardDeck) {
            this.player = player;
            this.deck = cardDeck;
            var selectCardsContainer = document.createElement('div');
            selectCardsContainer.innerHTML = deckTemplate;
            //selectCardsContainer.innerHTML = selectCardsSimpleTemplate;
            this.player.overlays.add(SELECT_CARDS_OVERLAY, selectCardsContainer, {
                visible: false,
                scaling: true,
                pointerEvents: false
            });

            var showSelectCards = document.createElement('div');
            showSelectCards.classList.add('deck');
            showSelectCards.innerHTML = showCardsButtonTemplate;
            this.player.overlays.add(SHOW_CARDS_BUTTON_OVERLAY, showSelectCards, {
                visible: false,
                scaling: true,
                pointerEvents: false
            });

            var $el = $('#baraja-el');

            this.ui = {
                show_select_cards_btn: document.querySelector('.show_select_cards_btn') || {},
                select_title: document.querySelector('.select_title') || {},
                select_cards: document.querySelector('.select_cards') || {},
                submit_cards: document.querySelector('.submit_cards') || {},

                //deck: document.querySelector('.deck_container'),

                baraja_el: $el,
                baraja_prev: document.querySelector('#nav-prev'),
                baraja_next: document.querySelector('#nav-next')

            };
            this.baraja;

            this.selectedCards = [];

        };

        Heir.inherit(CardDeckGui, EventEmitter);

        CardDeckGui.prototype.constructor = CardDeckGui;

        CardDeckGui.prototype.setCardDeck = function(deck) {
            this.deck = deck;
        };

        CardDeckGui.prototype.init = function () {
            console.log("init");
            var that = this;

            this.ui.show_select_cards_btn.addEventListener("click", function () {
                that.hideShowCardsButton();
                //that.showDeck();
                //that.selectHand();
            });

            //this.ui.deck.addEventListener("click", function () {
            //    //that.hideDeck();
            //});

            this.ui.submit_cards.addEventListener("click", function () {
                var results = that.getResultsFromElements(that.selectedCards);
                that.emit("selectedCards", results);
                console.log("selected cards", results);
                for (var i = 0; i < that.selectedCards.length; i++) {
                    that.selectedCards[i].classList.remove("clickCard");
                }
                that.baraja.close();
                setTimeout(function() {
                    that.hideSelectCards();
                    that.ui.baraja_el.empty();
                    that.ui.submit_cards.classList.add("disabled");
                    that.selectedCards = [];
                    //that.showShowCardsButton();
                }, 1000);

            });


            //this.selectHand();
            //this.showShowCardsButton();
        };

        CardDeckGui.prototype.hideSelectCards = function () {
            console.log("hide deck");
            this.player.overlays.hide(SELECT_CARDS_OVERLAY);
            //this.showSelectCards.classList.add('hidden');
        };

        CardDeckGui.prototype.showSelectCards = function () {
            console.log("show deck");
            //this.showSelectCards.classList.remove('hidden');
            this.player.overlays.show(SELECT_CARDS_OVERLAY);
            this.baraja.fan({
                speed: 600,
                //easing: 'ease-in',
                //easing: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
                easing: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
                range: 20,
                direction: 'right',
                origin: {x: 50, y: 200},
                center: false,
                translation: 500
            });
        };

        CardDeckGui.prototype.hideShowCardsButton = function () {
            console.log("hide button");
            this.player.overlays.hide(SHOW_CARDS_BUTTON_OVERLAY);
        };

        CardDeckGui.prototype.showShowCardsButton = function () {
            console.log("show button");
            this.player.overlays.show(SHOW_CARDS_BUTTON_OVERLAY);
        };

        CardDeckGui.prototype.selectHand = function(name) {
            this.showDeck(2, 'Pick Cards For ' + name);
        };

        CardDeckGui.prototype.selectFlop = function () {
            this.showDeck(3, 'Pick 3 Cards For The Flop');
        };

        CardDeckGui.prototype.selectForthStreet = function () {
            this.showDeck(1, 'Pick Card For The Forth Street');
        };

        CardDeckGui.prototype.selectRiver = function () {
            this.showDeck(1, 'Pick 1 Card For The River Card');
        };

        CardDeckGui.prototype.setSelection = function (amountToSelect) {
            var cards = document.querySelectorAll('.card');
            var that = this;
            this.counter = 0;
            for (var i = 1; i < cards.length; i++) {
                cards[i].addEventListener('click', function () {
                    if ($.inArray(this, that.selectedCards) > -1) {
                        var index = $.inArray(this, that.selectedCards);
                        this.classList.remove("clickCard");
                        that.selectedCards.splice(index, 1);
                        that.counter--;
                        that.ui.submit_cards.classList.add("disabled");
                    }
                    else {
                        if (that.counter >= amountToSelect) {
                            that.selectedCards[0].classList.remove("clickCard");
                            that.selectedCards.shift();
                            that.counter--;
                            that.ui.submit_cards.classList.add("disabled");
                        }
                        this.classList.add("clickCard");
                        that.selectedCards.push(this);
                        that.counter++;
                        if (that.counter >= amountToSelect){
                            that.ui.submit_cards.classList.remove("disabled");
                        }
                    }
                });
            }
        };

        CardDeckGui.prototype.showDeck = function (amountToSelect, submitText) {
            this.ui.submit_cards.textContent = submitText;
            var deck = this.deck.get();
            // append back
            var card = document.createElement('li');
            card.className = "card card_back";
            this.ui.baraja_el[0].appendChild(card);
            // append all cards
            for (var i = 0; i < (deck.length); i++) {
                var card = document.createElement('li');
                card.className = "card card_" + deck[i];
                this.ui.baraja_el[0].appendChild(card);
            }

            this.baraja = this.ui.baraja_el.baraja();
            //this.baraja.add( $('<li class="card card_S2"></li><li class="card card_S3"></li><li class="card card_S4"></li><li class="card card_S5"></li><li class="card card_S6"></li>'));

            this.setSelection(amountToSelect);

            this.showSelectCards();
        };

        CardDeckGui.prototype.getResultsFromElements = function(liArray) {
            var results = [];
            for (var i = 0; i < liArray.length; i++) {
                results[i] = liArray[i].classList[1].split("_")[1];
            }
            return results
        };

        return CardDeckGui;
    })
;
