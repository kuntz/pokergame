define([], function() {

    var cardDeck = function () {
        this.deck = [];
        var suits = ['C', 'D', 'H', 'S']; //Heart, Spade, Clubs, Diamond
        var cards = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
        for (var i = 0; i < suits.length; i++) {
            for (var j = 0; j < cards.length; j++) {
                this.deck.push(suits[i] + cards[j]);
            }
        }
    };

    cardDeck.prototype.get = function (cards) {
        if (!cards) {
            return this.deck;
        }
        if (!Array.isArray(cards)) {
            cards = [cards];
        }
        for(var i = 0 ; i < cards.length; i ++) {
            var card = cards[i];
            var index = this.deck.indexOf(card);
            if (index > -1) {
                this.remove(card);
            }
            else {
                return undefined;
            }
        }
    };

    cardDeck.prototype.add = function (card) {
        this.deck.push(card);
    };

    cardDeck.prototype.remove = function (card) {
        var index = this.deck.indexOf(card);
        if (index > -1) {
            this.deck.splice(index, 1);
        }
    };

    cardDeck.prototype.removeCards = function (cards) {
        for(var i = 0 ; i < cards.length; i ++) {
            this.remove(cards[i]);
        }
    };


        return cardDeck;
});
