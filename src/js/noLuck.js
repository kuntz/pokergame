'use strict';

define(['js/CardDeckGui', 'js/cardDeck', 'js/handRanker'], function(CardDeckGui, CardDeck) {
    var NoLuck = function(options) {
        this.player = options.player;

        this.cardDeck = new CardDeck();
        this.someElement = document.createElement('div');
        this.someElement.setAttribute('style', 'position:absolute;top:0;left:0;width:100%;height:100%;cursor:pointer');
        this.player.overlays.add('startToPlay', this.someElement);

        this.cardDeckGui = new CardDeckGui(this.player, this.cardDeck);
        this.cardDeckGui.init();
        this.cardDeckGui.on("selectedCards", function(cards) {
            this.cardDeck.removeCards(cards);
            cards = this.convertCard(cards);
            this.player.play();
            console.log(cards, this.currentMove);
            if (this.currentMove === 'dealToPlayer') {
                this.players[this.currentPlayer].cards = cards;
            }
            else {
                this.boardCards = _.union(this.boardCards, cards);
            }
         }.bind(this));

        // Set Data structures.
        this.resetLocals();

    };

    NoLuck.prototype.convertCard = function(cards) {
        console.log(cards);
        var convertCards = [];
        for (var i = 0; i < cards.length ; i ++) {
            var suit = cards[i].charAt(0).toLowerCase();
            var rank = cards[i].length === 2 ? cards[i].charAt(1) + cards[i].charAt(2) : cards[i].charAt(1);
            var newCard = rank + suit;
            convertCards.push(newCard);
        }
        console.log(convertCards);
        return convertCards;
    }

    NoLuck.prototype.resetLocals = function() {
        this.players = {
          bob: {
            cards: []
          },
          carol: {
            cards: []
          },
          ilana: {
            cards: []
          },
          alex: {
            cards: []
          }
        };
        this.boardCards = [];
        this.foldedPlayers = [];
        this.someoneRaisedThisTurn = false;
        this.someoneFoldedThisTurn = false;
        this.cardDeck = new CardDeck();
        this.cardDeckGui.setCardDeck(this.cardDeck);
    };

    NoLuck.prototype.constructor = NoLuck;

    NoLuck.prototype.addCard = function(pokerPlayer, card) {
        this.player[pokerPlayer].push(card);
    };

    NoLuck.prototype.setStartScreen = function() {
        // this.someElement = document.createElement('div');
        // this.someElement.setAttribute('style', 'position:absolute;top:0;left:0;width:100%;height:100%;cursor:pointer');
        // this.player.overlays.add('startToPlay', this.someElement);
        // this.someElement.addEventListener('click', function(){
        //     this.player.play();
        //     this.player.overlays.hide('startToPlay');
        // }.bind(this));
    };

    NoLuck.prototype.addNodeListeners = function(nodes) {
        var currNode;
        for (var i = 0; i < nodes.length ; i++) {
            (function(indx) {
                if (nodes[indx].id === '01A_Intro') {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('timeupdate:3.5', function() {
                        this.player.overlays.show('startToPlay');
                        this.player.pause();
                        this.someElement.addEventListener('click', function(){
                            this.player.play();
                            this.player.overlays.hide('startToPlay');
                        }.bind(this));
                    }.bind(this));
                }
                if (nodes[indx].id === '02A_Loop_of_whole_table') {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('nodestart', function(){
                        this.player.audio.play('partial_soundtrack');
                    }.bind(this));
                }
                if (nodes[indx].dealToPlayer) {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('nodestart', function() {
                        this.player.pause();
                        this.currentMove = 'dealToPlayer';
                        this.currentPlayer = nodes[indx].player;
                        console.log('selecyHand', this.currentPlayer);
                        this.cardDeckGui.selectHand(this.currentPlayer);
                    }.bind(this));
                    return;
                }
                if (nodes[indx].dealFlop) {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('timeupdate:14', function() {
                        this.player.pause();
                        this.currentMove = 'dealFlop';
                        currNode = this.player.repository.get(nodes[indx].id);
                        this.cardDeckGui.selectFlop();
                    }.bind(this));
                    return;
                }
                if (nodes[indx].dealStreet) {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('timeupdate:10.8', function() {
                        this.player.pause();
                        this.currentMove = 'dealStreet';
                        currNode = this.player.repository.get(nodes[indx].id);
                        this.cardDeckGui.selectForthStreet();
                    }.bind(this));
                    return;
                }
                if (nodes[indx].dealRiver) {
                    currNode = this.player.repository.get(nodes[indx].id);
                    currNode.once('timeupdate:12.2', function() {
                        this.player.pause();
                        this.currentMove = 'dealRiver';
                        currNode = this.player.repository.get(nodes[indx].id);
                        this.cardDeckGui.selectRiver();
                    }.bind(this));
                    return;
                }

            }.bind(this))(i);
        }
    };

    NoLuck.prototype.getDecisionFunctions = function(node) {
        var decisionFunction = function() {
            return undefined;
        };
        switch(node.type) {
            case 'dealToPlayer':
                return function(parent, children) {
                    console.log('dealToPlayer');
                    if (this.players[node.player].cards.length < 2) {
                        return parent;
                    }
                    this.player.play();
                    var currentPlayerRank = this.getPlayerRank(node.player);
                    if (currentPlayerRank < 5) {
                        return (children[0].id.indexOf('sad') > -1) ? children[0].id : children[1].id;
                    }
                    else {
                        return (children[0].id.indexOf('happy') > -1) ? children[0].id : children[1].id;
                    }
                }.bind(this);


            case 'playAfterFlop':
                return function(parent, children) {
                    console.log('playAfterFlop', node.id);
                    if (this.boardCards.length < 3) {
                        return parent;
                    }

                    if (node.nextPlayer) {
                        var nextPlayer = node.nextPlayer;
                        var playerCurrScore = this.getPlayerRank(nextPlayer);
                        console.log(playerCurrScore);
                        // Alex can't raise!!
                        if (playerCurrScore > 3 && nextPlayer !== 'alex' && !this.someoneRaisedThisTurn) {
                            // Raise
                            this.someoneRaisedThisTurn = true;
                            return children[1].id;
                        }
                        if (playerCurrScore < 2 && !this.someoneFoldedThisTurn) {
                            // Fold
                            this.removePlayer(nextPlayer);
                            if (nextPlayer === 'alex') {
                                return children[1].id;
                            }
                            return children[2].id;
                        }
                        else {
                            // Bet
                            return children[0].id;
                        }
                    }
                    else { // This is alex turn, if he folds the game is over.
                        if(node.id.indexOf('ALEX_folds') > -1) {
                            // GAME OVER
                            return children[0].id;
                        }
                        else {
                            return children[1].id;
                        }
                    }
                }.bind(this);
            case 'opponentsFeelings':
                return function(parent, children) {
                    console.log('opponentsFeelings');
                    var opponentsRank = this.getPlayerRank('bob') + this.getPlayerRank('carol') + this.getPlayerRank('ilana');
                    console.log('opponentsRank is:', opponentsRank);
                    // Generally pleased
                    if (opponentsRank > 12) {
                        return children[0].id;
                    }
                    // Not pleased.
                    else {
                        return children[1].id;
                    }
                }.bind(this);

            case 'dealStreet':
                return function(parent, children) {
                    console.log('dealStreet');
                    console.log('this.someoneRaisedThisTurn: ', this.someoneRaisedThisTurn);
                    console.log('this.someoneFoldedThisTurn: ', this.someoneFoldedThisTurn);
                    if (this.boardCards.length < 4) {
                        return parent;
                    }
                    if (node.nextPlayers.length) {
                        for (var i = 0; i < node.nextPlayers.length ; i ++) {
                            if(this.players[node.nextPlayers[i]]) {
                                var playerName = node.nextPlayers[i];
                                console.log('playerName in street:', playerName);

                                var playerScore = this.getPlayerRank(playerName);
                                if (playerScore > 4 && !this.someoneRaisedThisTurn) {
                                    if (playerName === 'alex') {
                                        return children[node.playerNodesMap[playerName][0]];
                                    }
                                    // This player is raising the bet
                                    this.someoneRaisedThisTurn = true;
                                    return children[node.playerNodesMap[playerName][1]];
                                }
                                else if(playerScore < 2) {
                                    if (playerName === 'alex') {
                                        // Alex folds
                                        return children[node.playerNodesMap[playerName][1]];
                                    }
                                    else if (!this.someoneFoldedThisTurn) {
                                        this.removePlayer(playerName);

                                        return children[node.playerNodesMap[playerName][2]];
                                    }
                                    else {
                                        return children[node.playerNodesMap[playerName][0]];
                                    }
                                }
                                else {
                                    return children[node.playerNodesMap[playerName][0]];
                                }
                            }
                        }
                    }
                }.bind(this);
            case 'dealRiver':
                return function(parent, children) {
                    console.log('dealRiver');
                    console.log('this.someoneRaisedThisTurn: ', this.someoneRaisedThisTurn);
                    console.log('this.someoneFoldedThisTurn: ', this.someoneFoldedThisTurn);
                    if (this.boardCards.length < 5) {
                        return parent;
                    }
                    if (node.nextPlayers.length) {
                        for (var i = 0; i < node.nextPlayers.length ; i ++) {
                            if(this.players[node.nextPlayers[i]]) {
                                var playerName = node.nextPlayers[i];
                                var playerScore = this.getPlayerRank(playerName);
                                if (playerScore > 6) {
                                    if (playerName === 'alex' || this.someoneRaisedThisTurn) {
                                        return children[node.playerNodesMap[playerName][0]];
                                    }
                                    else if(!this.someoneRaisedThisTurn) {
                                        // This player is raising the bet
                                        return children[node.playerNodesMap[playerName][1]];
                                    }
                                }
                                else if(playerScore < 3 && !this.someoneFoldedThisTurn) {
                                    if (playerName === 'alex') {
                                        return children[node.playerNodesMap[playerName][0]];
                                    }
                                    this.removePlayer(playerName);
                                    if (this.foldedPlayers.length === 3 && node.playerNodesMap.endOfGame) {
                                            return children[node.playerNodesMap.endOfGame];
                                    }
                                    return children[node.playerNodesMap[playerName][2]];
                                }
                                else {
                                    return children[node.playerNodesMap[playerName][0]];
                                }
                            }
                        }
                    }
                }.bind(this);
            case 'beforeLoopOfTable':
                return function(parent, children) {
                    console.log('beforeLoopOfTable');
                    if (this.foldedPlayers.length === 0) {
                        return children[0];
                    }
                    if (this.foldedPlayers.indexOf('ilana') > -1) {
                        if (this.foldedPlayers.indexOf('bob') > -1) {
                            // without Ilana and bob
                            return children[6];
                        }
                        else if (this.foldedPlayers.indexOf('carol') > -1) {
                            // Without Ilana and carol
                            return children[5];
                        }
                        else {
                            // Without Ilana
                            return children[1];
                        }
                    }
                    if (this.foldedPlayers.indexOf('carol') > -1) {
                        if (this.foldedPlayers.indexOf('bob') > -1) {
                            // Without carol and bob
                            return children[4];
                        }
                        else {
                            // Without carol
                            return children[3];
                        }
                    }
                    if (this.foldedPlayers.indexOf('bob') > -1) {
                        // Without bob
                        return children[2];

                    }

                }.bind(this);
            case 'matchPoint':
                return function (parent, children) {
                    var winner;
                    if (node.alexFolded) {
                        this.removePlayer('alex');
                        winner = this.andTheWinnerIs();
                        if (winner === 'ilana') {
                            return children[0];
                        }
                        if (winner === 'carol') {
                            return children[1];
                        }
                        if (winner === 'bob') {
                            return children[2];
                        }
                    }
                    else {
                        winner = this.andTheWinnerIs();
                        if (winner === 'alex') {
                            return children[0];
                        }
                        if (winner === 'ilana') {
                            return children[1];
                        }
                        if (winner === 'carol') {
                            return children[2];
                        }
                        if (winner === 'bob') {
                            return children[3];
                        }
                    }
                }.bind(this);
            case 'newRound':
                return function() {
                    this.someoneRaisedThisTurn = false;
                    this.someoneFoldedThisTurn = false;
                    console.log('newRound!!!!', this.someoneFoldedThisTurn, this.someoneRaisedThisTurn);
                    return undefined;
                }.bind(this);

        }
        return decisionFunction;
    };

    // The rank is a number between 0-9
    NoLuck.prototype.getPlayerRank = function(pokerPlayer) {
        var playerCards = this.players[pokerPlayer].cards;
        var allCards = _.union(playerCards, this.boardCards);
        console.log(allCards);
        var score;
        if (allCards.length < 7) {
            var firstCard = {
                rank: (allCards[0].length === 2) ? allCards[0].charAt(0) : allCards[0].charAt(0)+allCards[0].charAt(1),
                suit: (allCards[0].length === 2) ? allCards[0].charAt(1) : allCards[0].charAt(2)
            };
            var secondCard = {
                rank: (allCards[1].length === 2) ? allCards[1].charAt(0) : allCards[1].charAt(0)+allCards[1].charAt(1),
                suit: (allCards[1].length === 2) ? allCards[1].charAt(1) : allCards[1].charAt(2)
            };
            console.log(firstCard, secondCard);
            if (firstCard.rank === secondCard.rank) {
                if (firstCard.rank >7 || ['K','Q', 'J','A'].indexOf(firstCard.rank) > -1) {
                    score = 7;
                }
                else {
                    score = 4;
                }
            }
            else if (firstCard.suit === secondCard.suit) {
                if (['K','Q', 'J','A'].indexOf(firstCard.rank) > -1) {
                    if (['K','Q', 'J','A'].indexOf(secondCard.rank) > -1) {
                        score = 8;
                    }
                    else {
                        score = 6;
                    }
                }
                else {
                    score = 4;
                }
            }
            else {
                score = 1;
            }
            console.log(pokerPlayer,' hand score is:', score);
            return score;
        }
        var hand = Hand.solve(allCards);
        console.log(pokerPlayer, 'has a hand: ', hand);
        return hand.rank;
    };

    NoLuck.prototype.andTheWinnerIs = function() {
        var solvedHands = [];
        for (var pokerPlayer in this.players) {
          if (this.players.hasOwnProperty(pokerPlayer)) {
            var bla = Hand.solve(this.players[pokerPlayer].cards);
            bla.playerName = pokerPlayer;
            solvedHands.push(bla);
          }
        }
        var winners = Hand.winners(solvedHands);
        return winners[0].playerName;
    };

    NoLuck.prototype.removePlayer = function(pokerPlayer) {
        this.someoneFoldedThisTurn = true;
        this.foldedPlayers.push(pokerPlayer);
        delete this.players[pokerPlayer];
    };

    // private functions
    NoLuck.prototype.getCards = function(cards) {
      console.log("The cards are ", cards);
    };

    return NoLuck;

});
