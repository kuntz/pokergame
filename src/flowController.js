define(['underscore', 'utils'],
	function(_, utils) {
		'use strict';

		// var player, app;

		var FlowController = function(interlude, appRef) {

			this.player = interlude.player;
			this.app = appRef;
			// we're assuming that the start screen is showing at this point
			utils.sendGaEvent('player','start-screen');
			_.bindAll(this, 'init', 'onStarted', 'onEnd', 'onSeekStop');
		};

		FlowController.prototype.constructor = FlowController;

		FlowController.prototype.init = function() {
			this.player.once('nodestart', this.onStarted);
		};

		FlowController.prototype.onStarted = function() {
	        console.log('loader onStarted');
	        utils.sendGaEvent('player', 'play');
	        if (this.player.analytics !== undefined) {
                // set project ID - need to be set by the helix / cluster ??? curretn set by the loader
                // proj.player.analytics.setGlobalProperty(projectID, settings.id);
                // increase projectInSession each time project started
                this.player.analytics.setGlobalProperty(
                    'projectInSession',
                    (this.player.analytics.getGlobalProperty('projectInSession') || 0) + 1
                );
                // track view
                this.player.analytics.view('active');
            }
	        this.app.onStarted();
	        this.player.on(this.player.events.ended, this.onEnd);
	        this.player.on('seek.stop', this.onSeekStop);
	    };

	    FlowController.prototype.onEnd = function() {
	    	if (this.player.analytics !== undefined) {
                this.player.analytics.viewEnd('completed');
            }
	        this.app.onEnd(true);
	        this.player.once('nodestart', this.onStarted);
	        this.player.off(this.player.events.ended, this.onEnd);
	        this.player.off('seek.stop', this.onSeekStop);
	    };

	    FlowController.prototype.onSeekStop = function() {
	    	if (this.player.analytics !== undefined) {
                this.player.analytics.viewEnd('interrupted');
            }
	        this.app.onEnd();
	        this.player.once('nodestart', this.onStarted);
	        this.player.off(this.player.events.ended, this.onEnd);
	        this.player.off('seek.stop', this.onSeekStop);
	    };

		return FlowController;
	}
);
