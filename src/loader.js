require.config({
    paths: {
        'underscore': 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min',
        'text': 'https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min',
        'InterludePlayer': 'https://storageinterludefm.global.ssl.fastly.net/players/html/master/4.6.0/91/player-with-plugins.min.gz',
        'when': 'https://cdnjs.cloudflare.com/ajax/libs/when/2.7.1/when'
    },
    waitSeconds: 100
});

require([
    'flowController',
    'utils',
    'when',
    'text!config/playerOptions.json',
    'InterludePlayer',
    'app.min',
    'js/loaderExtras'
],
function(FlowController, utils, when, _playerOptions, InterludePlayer, _app, loaderExtras) {
    'use strict';

    var loaderFunc = function(playerOptions, app) {
        var interlude = {};
        utils.sendGaEvent('player', 'player-loaded');
        var playerLoadedTime = new Date();

        if (!utils.checkEnvironment(InterludePlayer)) {
            window.addEventListener('message', utils.handleFallbackMessage);
            return;
        }

        var queryParams = window.InterludePlayer.environment.queryParams;

        if (queryParams.debug === 'true' && playerOptions.plugins && playerOptions.plugins.sentry && playerOptions.plugins.sentry.mode !== 'dev') {
            playerOptions.plugins.sentry.mode = 'qa';
        }

        loaderExtras.beforePlayerInit(playerOptions);

        // Handle the forceTech query param
        if (queryParams.forceTech) {
            playerOptions.EngineTech = playerOptions.EngineTech || {};
            playerOptions.EngineTech.forceTech = queryParams.forceTech;
        }

        // create a player and add it to the "interlude" global anchor
        interlude.player = window.player = new InterludePlayer('#player', playerOptions);
        
        //when player ready check if has  window.INTERLUDE_PLAYER_READY and it function then apply with the interlude player - used for VAST and VPAID 
        if(window.INTERLUDE_PLAYER_READY && typeof window.INTERLUDE_PLAYER_READY === 'function') {
            window.INTERLUDE_PLAYER_READY.apply(null, [interlude.player]);
        }

        // when the gui is ready, add the components
        interlude.player.gui.promise.done(function() {

            loaderExtras.afterPlayerInit(playerOptions);

            var flowController = new FlowController(interlude, app);

            // Start
            if (window.debug) {
                addListeners();
                interlude.player.initPlugin('debug', {});
            }

            if (window.InterludeEmbedbridge) {
                var dEmbed = when.defer();
                window.InterludeEmbedbridge.playerReady(interlude.player).then(function () {
                    dEmbed.resolve();
                });
            }
            else {
                var dEmbed = when.defer();
                dEmbed.resolve();
            }
            
            var d = when.defer();
            dEmbed.promise.then(function () {
                app.onInit(interlude, function (err) {
                    if (err) {
                        // TODO: what should we do in this case?
                        console.log('project initialization failed.');
                        throw 'project onInit() failed';
                    }
                    d.resolve();
                });

                // synchronous init can resolve now
                if (!app.settings.asyncInit) {
                    d.resolve();
                }
            });
            
            d.promise.then(function () {

                if (window.InterludeEmbedbridge) {
                    window.InterludeEmbedbridge.projectReady();
                }

                // track performance
                if (window.InterludeAnalytics) {
                    var trackFunc = window.InterludeAnalytics._track_?window.InterludeAnalytics._track_:window.InterludeAnalytics.track;
                    trackFunc('in:perfTimingL0', { appReady:(new Date()).getTime(), playerLoaded:playerLoadedTime.getTime() });
                }

                flowController.init();
                interlude.player.append((typeof app.head === 'function') ? app.head() : app.head);

                if (app.settings.autoPlay || queryParams.autoPlay || queryParams.autoplay || queryParams.auto_play) {
                    interlude.player.play();
                }
            });
        });

        if (interlude.player.videoTech() === 'hls') {
            utils.refreshHLS();
        }
    }

    if (window.InterludeEmbedbridge) {
        window.InterludeEmbedbridge.load(loaderFunc, JSON.parse(_playerOptions), _app);
    } else {
        loaderFunc(JSON.parse(_playerOptions), _app);
    }
}

);
